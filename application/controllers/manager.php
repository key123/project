<?php 
class Manager extends CI_controller{
	function __construct() {
		parent::__construct();
		$this->load->helper('url');
	}

	function index() {
		$data['template'] = 'frontend/manager';
		$data['title'] = 'title';
		$this->load->view('layout',$data);
	}
}