<?php 
class Project extends CI_controller{
	function __construct() {
		parent::__construct();
		$this->load->helper('url');
	}

	function index() {
		$data['template'] = 'frontend/project';
		$data['title'] = 'title';
		$this->load->view('layout',$data);
	}
}