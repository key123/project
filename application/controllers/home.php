<?php 
class Home extends CI_controller{
	function __construct() {
		parent::__construct();
		$this->load->helper('url');
	}

	function index() {
		$data['template'] = 'frontend/main_index';
		$data['title'] = 'title';
		$this->load->view('layout',$data);
	}
}