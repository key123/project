 		</div>
 		 
 		 <!-- jQuery Version 1.11.0 -->
	    <script src="//code.jquery.com/jquery.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

	    <!-- Metis Menu Plugin JavaScript -->
	    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/metisMenu.min.js"></script>

	    <!-- Morris Charts JavaScript -->
	    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael.min.js"></script>
	    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.min.js"></script>
	    <!-- // <script src="<?php echo base_url();?>assets/js/plugins/morris/morris-data.js"></script> -->

	    <!-- Custom Theme JavaScript -->
	    <script src="<?php echo base_url();?>assets/js/sb-admin-2.js"></script>

	    <script src="<?php echo base_url();?>assets/js/jquery.prettyPhoto.js"></script>
	    <script src="<?php echo base_url();?>assets/js/jquery.isotope.min.js"></script>
	    <script src="<?php echo base_url();?>assets/js/main.js"></script>
	    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    	<script src="<?php echo base_url();?>assets/js/chosen.jquery.js" type="text/javascript"></script>
 
        <script type="text/javascript">
          var config = {
            '.chosen-select'           : { },
            '.chosen-select-deselect'  : { allow_single_deselect:true },
            '.chosen-select-no-single' : { disable_search_threshold:10 },
            '.chosen-select-no-results': { no_results_text:'Oops, nothing found!' },
            '.chosen-select-width'     : { width:"95%" }
          }
          for (var selector in config) {
            $(selector).chosen(config[selector]);
          }
        </script>

 	</body>
 </html>