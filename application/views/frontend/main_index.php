<div id="page-wrapper">
<h1 class="text-center">Hello World</h1>
 
	<div class="row">
		<table class="table table-hover" border='1'>
			<thead>
				<tr>
					<th rowspan='2'>P.code</th>
					<th rowspan='2'>Project Name</th>
					<th rowspan='2'>Progress</th>
					<th rowspan='2'>Expected</th>
					<th rowspan='2'>Update day</th>
					<th rowspan='2'>Last Update</th>
					<th rowspan='2'>Name Of person</th>
					<th colspan='2'>Gross profit</th>
					<th colspan='2'>Details</th>
				</tr>
				<tr>
					<td>$</td>
					<td>Profit Rate</td>
					<td>Sheet</td>
					<td>Manger</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>abc123</td>
					<td>SEO</td>
					<td>90%</td>
					<td>95%</td>
					<td>20/10/2014 10:02:20</td>
					<td>20/10/2014 10:02:20</td>
					<td>Kimura San</td>
					<td>4000</td>
					<td>60%</td>
					<td><a href="<?php echo site_url()?>project/">Link Sheet Project</a></td>
					<td><a href="<?php echo site_url()?>manager">Link Manger</a></td>
				</tr>

				<tr>
					<td>abc123</td>
					<td>SEO</td>
					<td>90%</td>
					<td>95%</td>
					<td>20/10/2014 10:02:20</td>
					<td>20/10/2014 10:02:20</td>
					<td>Kimura San</td>
					<td>4000</td>
					<td>60%</td>
					<td><a href="<?php echo site_url()?>project/">Link Sheet Project</a></td>
					<td><a href="<?php echo site_url()?>manager">Link Manger</a></td> 
				</tr>

			</tbody>
		</table>
	</div>
 
</div>