<section id="portfolio" class="">
                            <ul class="portfolio-filter">
                                <li><a class="btn btn-default active" href="#" data-filter="*">All</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".success">Success</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".begin">Begin</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".warning">Waring</a></li>
                            </ul>/#portfolio-filter
                              <div class="row">
                                   <ul class="portfolio-items col-4">
                                        <li class="portfolio-item success">
                                          <div class="col-sm-12">
                                             <div class="panel panel-green">
                                                 <div class="panel-heading">
                                                     <div class="row">
                                                         <div class="col-sm-3">
                                                             <i class="fa fa-comments fa-5x"></i>
                                                         </div>
                                                         <div class="col-sm-9 text-right">
                                                             <div class="huge">26</div>
                                                             <div>New Comments!</div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <a href="#">
                                                     <div class="panel-footer">
                                                         <span class="pull-left">View Details</span>
                                                         <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                        </li>
          
                                        <li class="portfolio-item success">
                                          <div class="col-sm-12">
                                             <div class="panel panel-green">
                                                 <div class="panel-heading">
                                                     <div class="row">
                                                         <div class="col-sm-3">
                                                             <i class="fa fa-comments fa-5x"></i>
                                                         </div>
                                                         <div class="col-sm-9 text-right">
                                                             <div class="huge">26</div>
                                                             <div>New Comments!</div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <a href="#">
                                                     <div class="panel-footer">
                                                         <span class="pull-left">View Details</span>
                                                         <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                        </li>
          
                                        <li class="portfolio-item begin">
                                          <div class="col-sm-12">
                                             <div class="panel panel-primary">
                                                 <div class="panel-heading">
                                                     <div class="row">
                                                         <div class="col-sm-3">
                                                             <i class="fa fa-comments fa-5x"></i>
                                                         </div>
                                                         <div class="col-sm-9 text-right">
                                                             <div class="huge">26</div>
                                                             <div>New Comments!</div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <a href="#">
                                                     <div class="panel-footer">
                                                         <span class="pull-left">View Details</span>
                                                         <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                        </li>
          
                                        <li class="portfolio-item warning">
                                          <div class="col-sm-12">
                                             <div class="panel panel-red">
                                                 <div class="panel-heading">
                                                     <div class="row">
                                                         <div class="col-sm-3">
                                                             <i class="fa fa-comments fa-5x"></i>
                                                         </div>
                                                         <div class="col-sm-9 text-right">
                                                             <div class="huge">26</div>
                                                             <div>New Comments!</div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <a href="#">
                                                     <div class="panel-footer">
                                                         <span class="pull-left">View Details</span>
                                                         <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                        </li>
          
                                        <li class="portfolio-item warning">
                                          <div class="col-sm-12">
                                             <div class="panel panel-red">
                                                 <div class="panel-heading">
                                                     <div class="row">
                                                         <div class="col-sm-3">
                                                             <i class="fa fa-comments fa-5x"></i>
                                                         </div>
                                                         <div class="col-sm-9 text-right">
                                                             <div class="huge">26</div>
                                                             <div>New Comments!</div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <a href="#">
                                                     <div class="panel-footer">
                                                         <span class="pull-left">View Details</span>
                                                         <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                        </li>
          
                                        <li class="portfolio-item warning">
                                          <div class="col-sm-12">
                                             <div class="panel panel-red">
                                                 <div class="panel-heading">
                                                     <div class="row">
                                                         <div class="col-sm-3">
                                                             <i class="fa fa-comments fa-5x"></i>
                                                         </div>
                                                         <div class="col-sm-9 text-right">
                                                             <div class="huge">26</div>
                                                             <div>New Comments!</div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <a href="#">
                                                     <div class="panel-footer">
                                                         <span class="pull-left">View Details</span>
                                                         <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                        </li>
          
                                 </ul>
                              </div>
                        </section> 