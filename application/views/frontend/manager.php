<div id="page-wrapper">
	<h1 class="text-center">Manger Sheets</h1>
	<div class="sheet-manger row">
		<ul class="list-stage list-unstyled">
			<ul class="list-unstyled col-md-12 control">
				<li class='col-md-3'><button type="button" class="btn btn-primary button-stage-control">Add Stage</button></li>
			</ul>
			 <?php for($j = 0 ; $j<2; $j++) {?>
			<li class='stage-element'>
				<div class="stage col-md-1"> Stage 01 </div>
				<div class="stage-percent col-md-1"> 70% </div>
				<div class="col-md-10">
					<ul class="list-detail list-unstyled row">
						<?php for($i = 0 ; $i<2; $i++) {?>
						<li class='list-detail-element col-md-12'>
							<div class="col-md-7">
								<p class='content-detail'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat consectetur corporis alias, culpa distinctio iure, doloribus, minus earum totam esse ullam aliquid repudiandae odit pariatur porro neque rerum enim nam?</p>
							</div>

							<div class="content-percent-detail-plan col-md-1"> 15% </div>

							<div class="content-percent-detail-progress col-md-1"> 50%	</div>

							<div class="content-percent-detail-member col-md-3 text-right"> Nguyễn Thị Minh Hiếu </div>
						</li>
						<?php } ?> 
						<li class='list-status-element col-md-12'>
							<div class="col-md-10">Satus</div>
  							<div class="status col-md-2 text-right">99%</div>
						</li>
						<li>
							<div class="col-md-12">
  								<button type="button" class="btn btn-primary button-detail-control">Add Detail</button>
  							</div>
						</li>
					</ul>
				</div>
			</li>
			 <?php } ?> 

		</ul>
	</div>
</div>