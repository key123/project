<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header text-center">Project</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

          <?php // $this->load->view('frontend/portfolio');?>
            <div class="row">

            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#home" role="tab" data-toggle="tab">Detail</a></li>
              <li><a href="#target" role="tab" data-toggle="tab">Target</a></li>
              <li><a href="#stages" role="tab" data-toggle="tab">Stages</a></li>
              <li><a href="#problem" role="tab" data-toggle="tab">Problem</a></li>
              <li><a href="#customer" role="tab" data-toggle="tab">Customer</a></li>
              <li><a href="#profit" role="tab" data-toggle="tab">Profit</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home">
                <!-- Detail-->
                  <div class="col-lg-12">
                      <h2 class="text-center">Detail</h2>
                      <div class="panel-body">
                            <ul class="timeline">
                              <?php for($i = 0 ; $i<2; $i++) {?>
                                 <li class="timeline">
                                      <div class="timeline-badge success"><i class="fa fa-credit-card"></i>
                                      </div>
                                      <div class="timeline-panel">
                                          <div class="timeline-heading">
                                              <h4 class="timeline-title">Lorem ipsum dolor</h4>
                                          </div>
                                          <div class="timeline-body">
                                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolorem quibusdam, tenetur commodi provident cumque magni voluptatem libero, quis rerum. Fugiat esse debitis optio, tempore. Animi officiis alias, officia repellendus.</p>
                                          </div>
                                      </div>
                                  </li>
                                  <li class="timeline-inverted">
                                      <div class="timeline-badge warning"><i class="fa fa-graduation-cap"></i>
                                      </div>
                                      <div class="timeline-panel">
                                          <div class="timeline-heading">
                                              <h4 class="timeline-title">Lorem ipsum dolor</h4>
                                          </div>
                                          <div class="timeline-body">
                                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt obcaecati, quaerat tempore officia voluptas debitis consectetur culpa amet, accusamus dolorum fugiat, animi dicta aperiam, enim incidunt quisquam maxime neque eaque.</p>
                                          </div>
                                      </div>
                                  </li>
                                 <?php } ?>
                              </ul>
                         </div>
                  </div>   <!-- End of Detail-->
              </div>
              <div class="tab-pane" id="target">
                <!-- Target -->
                <div class="col-md-12" style='margin-top : 50px; padding-bottom : 50px;'>
                    <?php for($i = 0 ; $i<2; $i++) {?>
                    <div class="col-lg-4">
                      <div class="panel panel-primary">
                        <div class="panel-heading">
                            Target one
                        </div>
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                        </div>
                        <div class="panel-footer">
                            Time Expected
                        </div>
                      </div>
                    </div>
                    

                    <div class="col-lg-4">
                      <div class="panel panel-yellow">
                        <div class="panel-heading">
                            Target two
                        </div>
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                        </div>
                        <div class="panel-footer">
                            Time Expected
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="panel panel-green">
                        <div class="panel-heading">
                            Target three
                        </div>
                        <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                        </div>
                        <div class="panel-footer">
                            Time Expected
                        </div>
                      </div>
                    </div>

                    <?php }?>
                </div>
                <!-- End of targer-->
              </div>
              <div class="tab-pane" id="stages">
                <!-- Stages -->
                <div class="row" style='margin: 50px 0px;'>
                  <ul class="list-stage list-unstyled">
                    <?php for($j = 0 ; $j<2; $j++) {?>
                    <li class='stage-element'>
                      <div class="stage col-xs-2 col-sm-2 col-md-2 col-lg-2"> Stage 01 </div>
                      <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <ul class="list-detail list-unstyled">
                          <?php for($i = 0 ; $i<2; $i++) {?>
                          <li class='list-detail-element'>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <p class='content-detail'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat consectetur corporis alias, culpa distinctio iure, doloribus, minus earum totam esse ullam aliquid repudiandae odit pariatur porro neque rerum enim nam?</p>
                            </div>
                          </li>
                          <?php }?>
                        </ul>
                      </div>
                    </li>
                   <?php }?>
                  </ul>
                </div>
                <!-- End of stages -->
              </div>
              <div class="tab-pane" id="problem">
                <!-- Problem -->
                <div class="col-md-12" style='margin: 50px 0px;'>
                  <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Problem Pannel
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <!-- Nếu có câu trả lời là vấn đề được giải quyết  -->
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">Problem 1</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                                <!-- Nếu không có câu trả lời là vấn đề được được giải quyết => panel-body rỗng  --> 
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">Problem #2</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            waitting ....
                                        </div>
                                    </div>
                                </div>
                                 <!-- Nếu có câu trả lời là vấn đề được giải quyết  -->
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">Problem 3 #</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#23" class="collapsed">Problem #2</a>
                                        </h4>
                                    </div>
                                    <div id="23" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            waitting ....
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#24" class="collapsed">Problem #2</a>
                                        </h4>
                                    </div>
                                    <div id="24" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            waitting ....
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                </div>
                <!-- End of Problem -->
              </div>
              <div class="tab-pane" id="customer">
                <!-- Information customer -->
                   <div class="row" style='margin: 50px 0px;'>
                     <ul class="list-customer list-unstyled">
                         <!-- Company -->
 
 
                        <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Company Name</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos dicta tempora nesciunt modi fugiat et quidem itaque! Consequuntur dolores, ullam quae doloribus odio, quia esse quos maxime eos porro, ipsa!</b></div>
                       </li>

                        <!-- Address -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Address Company</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><address>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</address></div>
                       </li>

                         <!-- Name of person -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Name of person in charge</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">Lorem ipsum dolor sit amet, consectetur adipisicing elit.  </div>
                       </li>

                          <!-- Telephone -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><abbr title="">Telephone</abbr></div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">0511.3113</div>
                       </li>

                          <!-- Email -->
                        <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Email</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">jonh.doe@gmail.com</div>
                       </li>
                          
                         <!-- Figure of RelationShip --> 
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12" style='min-height : 300px;'>
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Figure of RelationShips </div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">Post one Image</div>
                       </li>

                         <!-- Project member -->
 
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Selection of Project member</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>

                        <!-- Communicator -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Comnunicator</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>

                        <!-- Chief Admnistrator -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Chief Administrator</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>

                        <!-- Project leader -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Project leader</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>

                          <!-- Project manager -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Project manager</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>
                        
                          <!-- Traslator -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Translator</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>

                        <!-- Designer -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Designer</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>
                        
                        <!-- Programmer -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Programmer</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                            <ul class="row member-of-project list-unstyled">
                              <li class="list-member-of-project">Cu Tí</li>
                              <li class="list-member-of-project">Van Dat</li>
                              <li class="list-member-of-project">Minh Hieu</li>
                            </ul>
                         </div>
                       </li>
                        
                        <!-- Total -->
                       <li class="list-customer-detail col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Total</div>
                         <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right">10 </div>
                       </li>

                     </ul>
                   </div>
                <!-- End of Information customer -->
              </div>
              <div class="tab-pane" id="profit">
                <!-- Profit -->
                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style='margin: 50px 0px;'>
                     <h3 class="text-center">Expected work man-hour</h3>
                     <!-- Dòng 1-->
                     <ul class="list-unstyled row">
      
                      <!-- 2 dòng list cố định -->
                      <li class="list-head col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
                        <ul class="float-left list-time-person-step list-unstyled col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
                          <li class="operation-time-profit col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" > Operation time</li>
                        </ul>
                      </li> 
    
                      <li class="list-head col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"> ### </div>
                        <ul class="float-left list-time-person-step list-unstyled col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
                          <li class="name-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> Person </li>
                          <li class="timefrom-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> Time From </li>
                          <li class="timeto-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> Time To </li>
                          <li class="days-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> Totalday </li>
                        </ul>

                         <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"> Total time </div>
                      </li>

                       <!-- kết thúc 2 dòng list cố định -->

                        <!-- bắt đầu vòng lặp step -->
                        <?php for($j = 1; $j<=3; $j++) {?>
                      <li class="list-detail-profit col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"> Step <?php echo $j?> </div>
                        
                        <ul class="float-left list-time-person-step list-unstyled col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
                          <?php for($i = 0; $i<3; $i++) {?>
                          <li class="name-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> Nguyễn Thị Minh Hiếu </li>
                          <li class="timefrom-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> 10/15/2014  8:00:00 AM </li>
                          <li class="timeto-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> 10/20/2014  12:30:00 PM </li>
                          <li class="days-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> 5.5 days </li>
                          <div class='clr'></div>
                            <?php } ?>
                        </ul>

                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"> 205.5 </div>

                      </li>

                        <?php } ?>
                        <!-- kết thúc vòng lặp các step -->

                      <li class="status-profit list-detail-profit col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"> Status</div>
                        <ul class="float-left list-time-person-step list-unstyled col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
                          <li class="name-profit col-xs-3 col-sm-3 col-md-3 col-lg-3"> </li>
                          <li class="timefrom-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">10/15/2014  8:00:00 AM</li>
                          <li class="timeto-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> 10/20/2014  12:30:00 PM  </li>
                          <li class="days-profit col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center"> 49.5days </li>
                        </ul>

                         <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"> result time </div>
                      </li> 

                    </ul>

                    <!-- Tính toán Profit -->
 
                    <div class="count-profit row">          
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-primary">
                          <div class="panel-heading"> Amount Received </div>
                          <div class="panel-body"><p class='amount-profit'>4000</p></div>
                        </div>
                      </div>

                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="panel panel-red">
                          <div class="panel-heading"> Labor cost </div>
                          <div class="panel-body"><p class='labor-cost-profit'>1000</p></div>
                        </div>
                      </div>

                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-red">
                          <div class="panel-heading"> Necessary expenses </div>
                          <div class="panel-body"><p class='necessary-cost-profit'>2500</p></div>
                        </div>
                      </div>

                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="panel panel-red">
                          <div class="panel-heading"> Total cost </div>
                          <div class="panel-body"><p class='total-cost-profit'>3500</p></div>
                        </div>
                      </div>

                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="panel panel-green">
                          <div class="panel-heading"> Gross profit </div>
                          <div class="panel-body"><p class='total-cost-profit'>500</p></div>
                        </div>
                      </div>
                    </div>

                   </div>


                <!-- End of Profit -->
              </div>
            </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->